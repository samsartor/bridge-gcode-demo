# Violin bridge G-code demo

[bridge.ipynb](bridge.ipynb) is a [jupyter notebook](https://jupyter.org/) which
takes in a CSV of points in 3D space and converts them (very stupidly) into
G-code. You can't run the code here in GitLab (only view a test of it I ran
earlier) but you can download it and try it yourself by following the [quick
start guide](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/).

[Download link](https://gitlab.com/samsartor/bridge-gcode-demo/-/archive/master/master.zip) (or look in the upper-left)
